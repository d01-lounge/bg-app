import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './containers/App';
import HomePage from './containers/HomePage';
import MailingListsPage from './containers/MailingListsPage';
import MailingListDetails from './containers/MailingListDetails';
import EventsPage from './containers/EventsPage';
import EventDetails from './containers/EventDetails';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="mailing-lists" component={MailingListsPage}/>
    <Route path="mailing-lists/:id" component={MailingListDetails}/>
    <Route path="mailing-lists/new" component={MailingListDetails}/>
    <Route path="events" component={EventsPage}/>
    <Route path="events/:id" component={EventDetails}/>
    <Route path="events/new" component={EventDetails}/>
  </Route>
);
