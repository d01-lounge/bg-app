import React, { PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mailingListActions from '../actions/mailingListActions';
import * as contextActions from '../actions/contextActions';
import Form from 'react-jsonschema-form';
import MailingListForm from '../formschemas/MailingList';
import Loader from '../components/Loader';
import { FETCH_LIST } from '../constants/actionTypes';
import Paper from 'material-ui/Paper';
import Card from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

class MailingListDetails extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      list: this.props.list && this.props.list.id ? Object.assign({}, this.props.list) : null,
      errors: {},
      saving: false
    };

    this.saveMailingList = this.saveMailingList.bind(this);
    this.onUpdateError = this.onUpdateError.bind(this);
    this.formChange = this.formChange.bind(this);
  }

  componentWillMount() {
    if (this.props.params.id === 'new') {
      this.setState({
        list: {
          name: '',
          subscribers: []
        }
      });

      return;
    }

    this.props.mailingListActions.fetchList(this.props.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.list.id && (!this.props.list.id || this.props.list.id !== nextProps.list.id)) {
      this.setState({
        list: Object.assign({}, nextProps.list)
      });
    }
  }

  saveMailingList(data) {
    this.props.mailingListActions.updateList(data.formData);
  }

  formChange(data) {
    this.setState({
      'list': Object.assign({}, data.formData)
    });
  }

  onUpdateError(data) {
    console.debug('err', data); // eslint-disable-line no-console
  }

  closeDetail() {
    browserHistory.push('/mailing-lists');
  }

  render() {
    let listTemplate;
    const form = MailingListForm;
    const list = this.state.list;

    if (list) {
      listTemplate = (
        <Card>
          <Form
            schema={form.formSchema}
            uiSchema={form.uiSchema}
            fields={form.fields}
            formData={list}
            onChange={this.formChange}
            onSubmit={this.saveMailingList}
            onError={this.onUpdateError}>
            <div className="formActions">
              <FlatButton label="Submit" type="submit" />
              <FlatButton label="Cancel" onClick={this.closeDetail} />
            </div>
          </Form>
        </Card>
      );
    }
    else {
      listTemplate = '';
    }

    return (
      <Paper>
        <Loader eventName={FETCH_LIST} />
        {listTemplate}
      </Paper>
    );
  }
}

MailingListDetails.propTypes = {
  list: PropTypes.object,
  params: PropTypes.object.isRequired,
  contextActions: PropTypes.object.isRequired,
  mailingListActions: PropTypes.object.isRequired
};

function parseList(list) {
  return list.id ? {
    id: list.id,
    name: list.name,
    subscribers: list.subscribers
  } : {};
}

function mapStateToProps(state) {
  return {
    list: parseList(state.list)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    contextActions: bindActionCreators(contextActions, dispatch),
    mailingListActions: bindActionCreators(mailingListActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MailingListDetails);

