import { expect } from 'chai';
import * as actionTypes from '../constants/actionTypes';
import AjaxStatusReducer from './ajaxStatusReducer';

describe('Reducers::AjaxStatusReducer', () => {
  it('should handle AJAX_CALL_START', () => {
    const action = { type: actionTypes.AJAX_CALL_START, actionType: 'stuff' };
    const expected = { stuff: 'loading' };
    expect(AjaxStatusReducer({}, action)).to.eql(expected);
  });
});
