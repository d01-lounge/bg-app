import { combineReducers } from 'redux';
import {routerReducer} from 'react-router-redux';
import { lists, list } from './mailingListsReducer';
import { events, event } from './eventsReducer';
import ajaxStatus from './ajaxStatusReducer';
import context from './contextReducer';
import file from './fileReducer';
import header from './headerReducer';
import users from './userReducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  lists: lists,
  list: list,
  events: events,
  event: event,
  ajaxStatus: ajaxStatus,
  context: context,
  file: file,
  header: header,
  users: users
});

export default rootReducer;
