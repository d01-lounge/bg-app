import * as types from '../constants/actionTypes';
import fetch from 'isomorphic-fetch';
import { ajaxCallStart, ajaxCallError, ajaxCallSuccess } from './ajaxStatusActions';
import API_CONFIG from '../constants/apiConfig';

export function fetchEvents() {
  return dispatch => {
    dispatch(ajaxCallStart(types.FETCH_EVENTS));

    return fetch(`${API_CONFIG.url}/events`)
      .then(response => response.json())
      .then(events => {
        dispatch(loadEvents(events));
        dispatch(ajaxCallSuccess(types.FETCH_EVENTS));
      })
      .catch((err) => {
        dispatch(ajaxCallError(types.FETCH_EVENTS, err));
      });
  };
}

export function loadEvents(events) {
  return {
    type: types.LOAD_EVENTS,
    events
  };
}

export function fetchEvent(id) {
  return dispatch => {
    dispatch(ajaxCallStart(types.FETCH_EVENT));

    return fetch(`${API_CONFIG.url}/events/${id}`)
      .then(response => response.json())
      .then(event => {
        dispatch(loadEvent(event));
        dispatch(ajaxCallSuccess(types.FETCH_EVENT));
      })
      .catch((err) => {
        dispatch(ajaxCallError(types.FETCH_EVENT, err));
      });
  };
}

export function loadEvent(event) {
  return {
    type: types.LOAD_EVENT,
    event
  };
}

export function updateEvent(event) {
  return dispatch => {
    dispatch(ajaxCallStart(types.UPDATE_EVENT));

    return fetch('/some/url')
      .then(response => response.json())
      .then(json => {
        return dispatch(loadEvent(json));
      });
  };
}
