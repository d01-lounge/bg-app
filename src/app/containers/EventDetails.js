import React, { PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Form from 'react-jsonschema-form';

import Paper from 'material-ui/Paper';
import Card from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import * as eventActions from '../actions/eventActions';
import * as contextActions from '../actions/contextActions';
import EventForm from '../formschemas/Event';
import Loader from '../components/Loader';
import { FETCH_EVENT } from '../constants/actionTypes';
import { MOBILE_BREAKPOINTS, TABLET } from '../constants/media';

class EventDetails extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      event: this.props.event && this.props.event.id ? Object.assign({}, this.props.event) : null,
      errors: {},
      saving: false
    };

    this.saveEvent = this.saveEvent.bind(this);
    this.onUpdateError = this.onUpdateError.bind(this);
    this.formChange = this.formChange.bind(this);
  }

  componentWillMount() {
    if (this.props.params.id === 'new') {
      this.setState({
        event: {
          title: '',
          description: '',
          image: '',
          location: {
            lat: '',
            lng: ''
          },
          mailingList: '',
          start: '',
          end: ''
        }
      });

      return;
    }

    this.props.eventActions.fetchEvent(this.props.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.event && nextProps.event.id && (!this.props.event.id || this.props.event.id !== nextProps.event.id)) {
      this.setState({
        event: Object.assign({}, nextProps.event)
      });
    }
  }

  saveEvent(data) {
    this.props.eventActions.updateEvent(data.formData);
  }

  formChange(data) {
    this.setState({
      'event': Object.assign({}, data.formData)
    });
  }

  onUpdateError(data) {
    console.debug('err', data); // eslint-disable-line no-console
  }

  closeDetail() {
    browserHistory.push('/events');
  }

  render() {
    let eventTemplate;
    const form = EventForm;
    const event = this.state.event;
    const wrapperStyle = {
      maxWidth: MOBILE_BREAKPOINTS[TABLET]
    };

    if (event) {
      eventTemplate = (
        <Card>
          <Form
            schema={form.formSchema}
            uiSchema={form.uiSchema}
            fields={form.fields}
            formData={event}
            onChange={this.formChange}
            onSubmit={this.saveEvent}
            onError={this.onUpdateError}>
            <div className="formActions">
              <FlatButton label="Submit" type="submit" />
              <FlatButton label="Cancel" onClick={this.closeDetail} />
            </div>
          </Form>
        </Card>
      );
    }
    else {
      eventTemplate = '';
    }

    return (
      <Paper style={wrapperStyle}>
        <Loader eventName={FETCH_EVENT} />
        {eventTemplate}
      </Paper>
    );
  }
}

EventDetails.propTypes = {
  event: PropTypes.object,
  params: PropTypes.object.isRequired,
  contextActions: PropTypes.object.isRequired,
  eventActions: PropTypes.object.isRequired
};

function parseEvent(event) {
  return event.id ? {
    id: event.id,
    title: event.title,
    description: event.description,
    image: event.image,
    location: event.location,
    mailingList: event.mailingList,
    start: event.start,
    end: event.end
  } : {};
}

function mapStateToProps(state) {
  return {
    event: parseEvent(state.event)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    contextActions: bindActionCreators(contextActions, dispatch),
    eventActions: bindActionCreators(eventActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EventDetails);

