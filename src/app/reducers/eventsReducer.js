import * as types from '../constants/actionTypes';
import initialState from './initialState';

export function events(state = initialState.events, action) {
	switch (action.type) {
		case types.LOAD_EVENTS:
			return action.more ? [...state, ...action.events] : action.events;
		default:
			return state;
	}
}

export function event(state = initialState.event, action) {
	switch (action.type) {
		case types.LOAD_EVENT:
			return Object.assign({}, action.event);
		default:
			return state;
	}
}
