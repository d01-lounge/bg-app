import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/mailingListActions';
import {ListItem} from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import ImageEdit from 'material-ui/svg-icons/image/edit';

class MailingList extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const list = this.props.list;
    return (
      <ListItem
        rightIconButton={<IconButton href={`/mailing-lists/${list.id}`}><ImageEdit color="#47A0DB" /></IconButton>}
        primaryText={list.name}
        secondaryText={`${list.subscribers.length} members`} />
    );
  }
}

MailingList.propTypes = {
  list: PropTypes.object.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(MailingList);
