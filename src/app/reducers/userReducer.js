import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function (state = initialState.users, action) {
  switch (action.type) {
    case types.LOAD_USERS:
      return [].concat(action.users);
    default:
      return state;
  }
}
