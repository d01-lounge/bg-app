import * as types from '../constants/actionTypes';

function getAjaxCall(action, type, response) {
  let call = {};
  const actionType = type || (response ? response.actionType : '');

  switch (action) {
    case types.AJAX_CALL_START:
      call[actionType] = 'loading';
      break;
    case types.AJAX_CALL_END:
      call[actionType] = 'ready';
      break;
    case types.AJAX_CALL_ERROR:
      call[actionType] = response.err;
      break;
  }

  return call;
}

export default function context(state = {}, action) {
  return Object.assign({}, state, getAjaxCall(action.type, action.actionType, action.response));
}
