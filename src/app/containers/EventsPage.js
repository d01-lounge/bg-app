import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { truncate } from 'underscore.string';
import Masonry from 'react-masonry-component';

import { Card, CardTitle, CardText, CardMedia, CardActions } from 'material-ui/Card';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import FlatButton from 'material-ui/FlatButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

import * as eventActions from '../actions/eventActions';
import * as contextActions from '../actions/contextActions';
import Loader from '../components/Loader';
import { FETCH_EVENTS } from '../constants/actionTypes';
import * as media from '../constants/media';

class EventsPage extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentWillMount() {
    this.props.contextActions.updateTitle('Events');
    this.props.contextActions.updateBackground('events');
    this.props.eventActions.fetchEvents();
  }

  render() {
    const wrapperStyle = {
      flex: '1 0 100%',
      minWidth: '100%'
    };
    const buttonStyle = {
      position: 'fixed',
      bottom: '1.5rem',
      right: '1.5rem'
    };
    const cardWidths = {
      [media.MOBILE]: '100%',
      [media.TABLET]: '50%',
      [media.DESKTOP]: '33.33%'
    };
    const cardWrapperStyle = {
      padding: '.375rem',
      width: cardWidths[this.props.media],
      boxSizing: 'border-box'
    };
    const cardTitleStyle = {
      fontSize: '1.125rem'
    };
    const cardSubtitleStyle = {
      paddingBottom: 0
    };
    const masonryOptions = {
      updateOnEachImageLoad: false
    };
    const overlayContentStyle = {
      background: 'rgba(0, 0, 0, 0.36)'
    };
    const cardTextStyle = {
      paddingTop: 0,
      paddingBottom: 0
    };

    return (
      <div style={wrapperStyle}>
        <Loader eventName={FETCH_EVENTS} />
        <Masonry options={masonryOptions}>
          {this.props.events.map(event => {
            return(
              <div style={cardWrapperStyle} key={event.id}>
                <Card>
                  <CardMedia
                    overlay={<CardTitle title={event.title} titleStyle={cardTitleStyle} />}
                    overlayContentStyle={overlayContentStyle}>
                    <img src={event.image} />
                  </CardMedia>
                  <CardTitle subtitle={event.start} style={cardSubtitleStyle} />
                  <CardText style={cardTextStyle}>
                    <p>{truncate(event.description, 100, '...')}</p>
                  </CardText>
                  <CardActions>
                    <FlatButton
                      href={`/events/${event.id}`}
                      label="edit"
                      primary={true}
                      />
                  </CardActions>
                </Card>
              </div>
            );
          })}
        </Masonry>
        <FloatingActionButton
          href="/events/new"
          mini={true}
          style={buttonStyle}>
          <ContentAdd />
        </FloatingActionButton>
      </div>
    );
  }
}

EventsPage.propTypes = {
  eventActions: PropTypes.object.isRequired,
  contextActions: PropTypes.object.isRequired,
  events: PropTypes.array.isRequired,
  ajaxStatus: PropTypes.object.isRequired,
  media: PropTypes.string.isRequired
};

function mapStateToProps(state) {
  return {
    events: state.events.map(event => Object.assign({}, event, {start: moment(event.start).format('ddd Do MMMM YYYY'), end: moment(event.end).format('ddd Do MMMM YYYY')})),
    ajaxStatus: state.ajaxStatus,
    media: state.context.media
  };
}

function mapDispatchToProps(dispatch) {
  return {
    eventActions: bindActionCreators(eventActions, dispatch),
    contextActions: bindActionCreators(contextActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EventsPage);
