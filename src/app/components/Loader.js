import React, { PropTypes } from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import { connect } from 'react-redux';
import { primary1Color } from '../constants/muiTheme';

class Loader extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const status = this.props.ajaxStatus[this.props.eventName];
    const className = `loader ${status}`;
    return (
      <RefreshIndicator
        className={className}
        size={48}
        left={0}
        top={0}
        loadingColor={primary1Color}
        status={status}
      />
    );
  }
}

Loader.propTypes = {
  eventName: PropTypes.string.isRequired,
  ajaxStatus: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    ajaxStatus: state.ajaxStatus
  };
}

export default connect(mapStateToProps)(Loader);
