import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function context(state = initialState.context, action) {
  switch (action.type) {
    case types.UPDATE_TITLE:
      return Object.assign({}, state, {title: action.title});
    case types.UPDATE_BACKGROUND:
      return Object.assign({}, state, {background: action.background});
    case types.UPDATE_DRAWER:
      return Object.assign({}, state, {drawer: action.visible});
    case types.UPDATE_MEDIA:
      return Object.assign({}, state, {media: action.media});
    default:
      return state;
  }
}
