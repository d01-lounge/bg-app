module.exports = function () {
  switch (process.env.NODE_ENV) {
    case 'production':
      return require('./webpack.config.prod.js');
    default:
      return require('./webpack.config.dev.js');
  }
};
