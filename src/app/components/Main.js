import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import debounce from 'lodash.debounce';

import * as contextActions from '../actions/contextActions';
import * as media from '../constants/media';

class Main extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.componentDidMount = this.componentDidMount.bind(this);
    this.updateMedia = this.updateMedia.bind(this);
  }

  componentDidMount() {
    this.updateMedia();
    window.addEventListener('resize', debounce(this.updateMedia, 150));
  }

  updateMedia() {
    const mobileTest = window.matchMedia(`(max-width: ${media.MOBILE_BREAKPOINTS[media.MOBILE]})`);
    const tabletTest = window.matchMedia(`(max-width: ${media.MOBILE_BREAKPOINTS[media.TABLET]})`);

    const newMedia = mobileTest.matches ? media.MOBILE : tabletTest.matches ? media.TABLET: media.DESKTOP;

    if (this.props.media !== newMedia) {
      this.props.contextActions.updateMedia(newMedia);
    }
  }

  render() {
    const mainStyle = {
      padding: '1.5rem .75rem',
      display: 'flex',
      flex: '1 0 100%',
      minWidth: '100%',
      justifyContent: 'center',
      alignItems: 'flex-start'
    };

    return (
      <div className="main" style={mainStyle}>
        {this.props.children}
      </div>
    );
  }
}

Main.propTypes = {
  children: PropTypes.object.isRequired,
  contextActions: PropTypes.object.isRequired,
  media: PropTypes.string.isRequired
};

function mapStateToProps(state) {
  return {
    media: state.context.media
  };
}

function mapDispatchToProps(dispatch) {
  return {
    contextActions: bindActionCreators(contextActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
