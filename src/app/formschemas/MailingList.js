import Subscriber from '../fields/Subscriber';

export default {
  formSchema: {
    type: 'object',
    required: ['name'],
    properties: {
      name: {
        type: 'string',
        title: 'Choose a name for the new mailing list',
        default: 'New Mailing List'
      },
      subscribers: {
        type: 'array',
        minItems: 1,
        default: [],
        items: {
          type: 'object',
          required: ['id'],
          properties: {
            name: {
              type: 'string'
            },
            email: {
              type: 'string'
            },
            id: {
              type: 'string'
            }
          }
        },
        title: 'Choose some subscribers'
      }
    }
  },
  uiSchema: {
    subscribers: {
      'ui:field': 'subscriber'
    }
  },
  fields: {
    'subscriber': Subscriber
  }
};
