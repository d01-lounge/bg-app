import * as types from '../constants/actionTypes';

export function updateTitle(title) {
  return dispatch => {
    dispatch(updateTitleSuccess(title));
  };
}

export function updateTitleSuccess(title) {
  return {
    type: types.UPDATE_TITLE,
    title
  };
}

export function updateBackground(background) {
  return dispatch => {
    dispatch(updateBackgroundSuccess(background));
  };
}

export function updateBackgroundSuccess(background) {
  return {
    type: types.UPDATE_BACKGROUND,
    background
  };
}

export function updateDrawer(visible) {
  return {
    type: types.UPDATE_DRAWER,
    visible
  };
}

export function updateMedia(media) {
  return {
    type: types.UPDATE_MEDIA,
    media
  };
}
