# Bright Giraffe

##MVP

* I can create a mailinglist consisting of e-mail addresses
	* mailing list
		* name
		* owner
		* subscribers (reference to users)
	* users are stored in a separate collection
		* name
		* email address
		* ...
* I can create an event with some info and link it to a mailing list
	* image
	* title
	* description
	* location
	* start
	* end
	* owner
	* reference to mailing list
	* invitees
		* user id
		* accepted/rejected
* I can unsubscribe from a mailing list
	* user is removed from list (1 list per event)
* I can see which mailing lists I am subscribed to
* I can log in as an admin to create/manage mailing lists and events
* Scheduled invite mail for events
	* I am able to send an invite (manually) from the event detail
		* Warning "invites already sent"
	* Includes accept/reject link
		* overwrite state

