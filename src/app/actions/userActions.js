import * as types from '../constants/actionTypes';
import fetch from 'isomorphic-fetch';
import { ajaxCallStart, ajaxCallError, ajaxCallSuccess } from './ajaxStatusActions';
import API_CONFIG from '../constants/apiConfig';

export function loadUsers(users) {
  return {
    type: types.LOAD_USERS,
    users
  };
}

export function findUsers(query = '') {
  return (dispatch) => {
    dispatch(ajaxCallStart(types.FIND_USERS));

    return fetch(`${API_CONFIG.url}/users?q=${query}`)
      .then(response => response.json())
      .then(users => {
        dispatch(loadUsers(users));
        dispatch(ajaxCallSuccess(types.LOAD_USERS));
      })
      .catch((err) => {
        dispatch(ajaxCallError(types.FIND_USERS, err));
      });
  };
}
