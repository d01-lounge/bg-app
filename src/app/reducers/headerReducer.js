import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function header(state = initialState.header, action) {
	switch (action.type) {
		case types.TOGGLE_SEARCH:
			return Object.assign({}, state, {search: !state.search});
		default:
			return state;
	}
}
