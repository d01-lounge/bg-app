import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Dropzone from 'react-dropzone';
import {Card, CardActions, CardMedia} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import * as fileActions from '../actions/fileActions';

class ImagePicker extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      image: {},
      dropzone: {}
    };

    this.onDrop = this.onDrop.bind(this);
    this.clearImage = this.clearImage.bind(this);
    this.openFileDialog = this.openFileDialog.bind(this);
  }

  componentWillMount() {
    this.setState({
      'formData': this.props.formData
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      formData: nextProps.file.url || '',
      image: {}
    }, () => this.props.onChange(this.state.formData));
  }

  openFileDialog() {
    if (this.dropzone) {
      this.dropzone.open();
    }
  }

  clearImage() {
    this.setState({
      formData: '',
      image: {}
    });
  }

  onDrop(files) {
    this.setState({
      image: files[0]
    });

    this.props.fileActions.uploadFile(files[0]);
  }

  render() {
    const cardStyle = {
      minHeight: '15rem',
      width: '100%',
      margin: '1.5rem auto'
    };
    const imgStyle = {
      width: '100%',
      height: '100%'
    };
    const actionsStyle = {
      background: '#FFF'
    };
    const buttonStyle = {
      color: '#ED7161'
    };
    const dropzoneProps = {
      id: 'uploadImage'
    };
    const uploadButtonStyle = {
      margin: '1.5rem 0'
    };
    const cardActions = this.state.formData ? <FlatButton label="Delete" onClick={this.clearImage} style={buttonStyle} /> : <p>Loading...</p>;
    const imageSrc = this.state.image.src || this.state.formData;
    const imgTemplate = imageSrc ? (
      <Card style={cardStyle}>
        <CardMedia>
          <img style={imgStyle} src={imageSrc} />
        </CardMedia>
        <CardActions style={actionsStyle}>
          {cardActions}
        </CardActions>
      </Card>
    ) : (
      <div>
        <Dropzone
          ref={(node) => { this.dropzone = node; }}
          accept="image/*"
          onDrop={this.onDrop}
          multiple={false}
          inputProps={dropzoneProps}
          style={{display: 'none'}} />
        <RaisedButton
          label="UPLOAD"
          labelColor="#FFFFFF"
          backgroundColor="#47A0DB"
          style={uploadButtonStyle}
          onClick={this.openFileDialog}
          />
      </div>
    );
    return (
      <div>
        <label htmlFor={dropzoneProps.id}>{this.props.schema.title}</label>
        {imgTemplate}
      </div>
    );
  }
}

ImagePicker.propTypes = {
  formData: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  schema: PropTypes.object.isRequired,
  file: PropTypes.object.isRequired,
  fileActions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    file: state.file
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fileActions: bindActionCreators(fileActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ImagePicker);
