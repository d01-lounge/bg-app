export const primary1Color = '#47A0DB';
export const primary2Color = '#2D3237';
export const primary3Color = '#FFFFFF';
export const accent1Color = '#71828C';
export const textColor = '#546979';
export const alternateTextColor = '#CCCCCC';
export const canvasColor = '#F3F3F3';
export const borderColor = '#CCCCCC';
export const disabledColor = '#151B20';
export const dividerColor = '#151B20';
export const shadowColor = '#000000';

export default {
  palette: {
    primary1Color: primary1Color,
    primary2Color: primary2Color,
    primary3Color: primary3Color,
    accent1Color: accent1Color,
    textColor: textColor,
    alternateTextColor: alternateTextColor,
    canvasColor: canvasColor,
    borderColor: borderColor,
    disabledColor: disabledColor,
    dividerColor: dividerColor,
    shadowColor: shadowColor
  }
};
