export const MOBILE = 'MOBILE';
export const TABLET = 'TABLET';
export const DESKTOP = 'DESKTOP';

export const MOBILE_BREAKPOINTS = {
  MOBILE: '480px',
  TABLET: '768px',
  DESKTOP: '960px'
};
