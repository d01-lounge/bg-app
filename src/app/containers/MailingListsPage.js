import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {List} from 'material-ui/List';
import Paper from 'material-ui/Paper';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

import * as mailingListActions from '../actions/mailingListActions';
import * as contextActions from '../actions/contextActions';
import MailingList from '../components/MailingList';
import Loader from '../components/Loader';
import { FETCH_LISTS } from '../constants/actionTypes';
import { MOBILE_BREAKPOINTS, TABLET } from '../constants/media';

class MailingListsPage extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentWillMount() {
    this.props.contextActions.updateTitle('Mailing Lists');
    this.props.contextActions.updateBackground('mailing-lists');
    this.props.mailingListActions.fetchLists();
  }

  render() {
    const buttonStyle = {
      position: 'fixed',
      bottom: '1.5rem',
      right: '1.5rem'
    };
    const listStyle = {
      padding: 0
    };
    const wrapperStyle = {
      maxWidth: MOBILE_BREAKPOINTS[TABLET],
      flex: 1
    };

    return (
      <Paper style={wrapperStyle}>
        <Loader eventName={FETCH_LISTS} />
        <List style={listStyle}>
          {this.props.lists.map(list => <MailingList key={list.id} list={list} />)}
        </List>
        <FloatingActionButton href="/mailing-lists/new" mini={true} style={buttonStyle}>
          <ContentAdd />
        </FloatingActionButton>
      </Paper>
    );
  }
}

MailingListsPage.propTypes = {
  mailingListActions: PropTypes.object.isRequired,
  contextActions: PropTypes.object.isRequired,
  lists: PropTypes.array.isRequired,
  ajaxStatus: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    lists: state.lists,
    ajaxStatus: state.ajaxStatus
  };
}

function mapDispatchToProps(dispatch) {
  return {
    mailingListActions: bindActionCreators(mailingListActions, dispatch),
    contextActions: bindActionCreators(contextActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MailingListsPage);
