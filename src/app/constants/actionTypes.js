export const FETCH_LISTS = 'FETCH_LISTS';
export const FETCH_LIST = 'FETCH_LIST';
export const ADD_LIST = 'ADD_LIST';
export const LOAD_LISTS = 'LOAD_LISTS';
export const LOAD_LIST = 'LOAD_LIST';

export const FETCH_EVENTS = 'FETCH_EVENTS';
export const LOAD_EVENTS = 'LOAD_EVENTS';
export const FETCH_EVENT = 'FETCH_EVENT';
export const LOAD_EVENT = 'LOAD_EVENT';
export const UPDATE_EVENT = 'UPDATE_EVENT';

export const UPDATE_DRAWER = 'UPDATE_DRAWER';
export const UPDATE_TITLE = 'UPDATE_TITLE';
export const UPDATE_BACKGROUND = 'UPDATE_BACKGROUND';
export const UPDATE_MEDIA = 'UPDATE_MEDIA';

export const TOGGLE_SEARCH = 'TOGGLE_SEARCH';

export const REMOVE_USER = 'REMOVE_USER';
export const FIND_USERS = 'FIND_USERS';
export const LOAD_USERS = 'LOAD_USERS';

export const AJAX_CALL_START = 'AJAX_CALL_START';
export const AJAX_CALL_END = 'AJAX_CALL_END';
export const AJAX_CALL_ERROR = 'AJAX_CALL_ERROR';

export const UPLOAD_FILE = 'UPLOAD_FILE';
export const LOAD_FILE = 'LOAD_FILE';
