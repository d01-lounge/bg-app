import React, { PropTypes } from 'react';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import moment from 'moment';

class SchemaDateTimePicker extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onChange = this.onChange.bind(this);
    this.parseDate = this.parseDate.bind(this);
  }

  componentWillMount() {
    this.setState({
      dateValue: this.props.formData ? new Date(this.props.formData) : new Date(),
      formData: this.props.formData
    });
  }

  parseDate(newValue, oldValue, type) {
    if (!oldValue && newValue) {
      return newValue;
    }

    if (!newValue && oldValue) {
      return oldValue;
    }

    const newDate = moment(newValue);
    const oldDate = oldValue ? moment(oldValue) : newDate;
    const newStart = moment(newValue).startOf('day');
    const oldStart = oldValue ? moment(oldValue).startOf('day') : newStart;

    let res = newDate;

    switch (type) {
      case 'time':
        res = oldStart.valueOf() + (newDate.valueOf() - newStart.valueOf());
        break;
      case 'date':
        res = newStart.valueOf() + (oldDate.valueOf() - oldStart.valueOf());
        break;
    }

    return new Date(res);
  }

  onChange(event, date, type) {
    const newValue = this.parseDate(date, this.state.formData, type);
    this.setState({
      dateValue: newValue,
      formData: newValue.toISOString()
    }, () => this.props.onChange(this.state.formData));
  }

  render() {
    const schema = this.props.schema;
    const now = this.props.formData ? typeof this.props.formData === 'string' ? new Date(this.props.formData) : this.props.formData : new Date();
    const fieldStyle = {
      width: '100%'
    };

    return (
      <div>
        <DatePicker
          floatingLabelText={schema.title}
          hintText={schema.title}
          container="inline"
          defaultDate={now}
          value={this.state.dateValue}
          onChange={(event, date) => this.onChange(event, date, 'date')}
          style={fieldStyle}
        />
        <TimePicker
          hintText={schema.title}
          defaultTime={now}
          value={this.state.dateValue}
          onChange={(event, date) => this.onChange(event, date, 'time')}
          style={fieldStyle}
        />
      </div>
    );
  }
}

SchemaDateTimePicker.propTypes = {
  formData: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  schema: PropTypes.object.isRequired
};

export default SchemaDateTimePicker;
