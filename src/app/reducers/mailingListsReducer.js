import * as types from '../constants/actionTypes';
import initialState from './initialState';

export function lists(state = initialState.lists, action) {
	switch (action.type) {
		case types.LOAD_LISTS:
			return action.more ? [...state, ...action.lists] : action.lists;
		case types.ADD_LIST:
			return [
				...state,
				Object.assign({}, action.list, {id: state.lists.length})
			];
		default:
			return state;
	}
}

export function list(state = initialState.list, action) {
	switch (action.type) {
		case types.LOAD_LIST:
			return Object.assign({}, action.list);
    case types.REMOVE_USER:
      return Object.assign({}, state, {subscribers: state.subscribers.filter(user => user.id !== action.id)});
		default:
			return state;
	}
}
