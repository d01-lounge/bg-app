import * as types from '../constants/actionTypes';
import fetch from 'isomorphic-fetch';
import {ajaxCallStart, ajaxCallError, ajaxCallSuccess} from './ajaxStatusActions';

const cloudinaryPreset = 'ssqvr76r';
const cloudinaryAPI = `https://api.cloudinary.com/v1_1/mrtom/image/upload`;

export function uploadFile(file) {
  return dispatch => {
    const data = new FormData();
    data.append('file', file);
    data.append('upload_preset', cloudinaryPreset);

    dispatch(ajaxCallStart(types.UPLOAD_FILE));

    return fetch(cloudinaryAPI, {
      method: 'POST',
      body: data
    })
      .then(response => response.json())
      .then(formData => {
          dispatch(ajaxCallSuccess(types.UPLOAD_FILE));
          dispatch(loadFile(formData));
      })
      .catch((err) => {
        dispatch(ajaxCallError(types.UPLOAD_FILE, err));
      });
  };
}

export function loadFile(file) {
  return {
    type: types.LOAD_FILE,
    file
  };
}
