import React, { PropTypes, Component } from 'react';
import Chip from 'material-ui/Chip';

class SelectedUsers extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentWillMount() {
    this.setState({
      users: this.mapUsers(this.props.users)
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      users: this.mapUsers(nextProps.users)
    });
  }

  mapUsers(users) {
    return users.map(user => Object.assign({key: user.id, label: `${user.name} (${user.email})`}));
  }

  renderUser(data) {
    const userStyle = {
      marginRight: '.5rem',
      marginBottom: '.75rem'
    };

    return (
      <Chip
        key={data.key}
        onRequestDelete={() => this.props.onRemoveUser(data.key)}
        style={userStyle}>
        {data.label}
      </Chip>
    );
  }

  render() {
    const users = this.state.users;
    const wrapperStyle = {
      display: 'flex',
      flexWrap: 'wrap'
    };
    return (
      <div style={wrapperStyle}>
        {users.map(this.renderUser, this)}
      </div>
    );
  }
}

SelectedUsers.propTypes = {
  users: PropTypes.array.isRequired,
  onRemoveUser: PropTypes.func
};

export default SelectedUsers;
