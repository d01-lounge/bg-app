import * as types from '../constants/actionTypes';

export function ajaxCallStart(actionType) {
  return {
    type: types.AJAX_CALL_START,
    actionType
  };
}

export function ajaxCallSuccess(actionType) {
  return {
    type: types.AJAX_CALL_END,
    actionType
  };
}

export function ajaxCallError(actionType, err) {
  return {
    type: types.AJAX_CALL_ERROR,
    response: {
      actionType: actionType,
      err: err
    }
  };
}
