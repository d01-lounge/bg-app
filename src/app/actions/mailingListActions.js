import * as types from '../constants/actionTypes';
import fetch from 'isomorphic-fetch';
import { ajaxCallStart, ajaxCallError, ajaxCallSuccess } from './ajaxStatusActions';
import API_CONFIG from '../constants/apiConfig';

export function fetchLists() {
  return dispatch => {
    dispatch(ajaxCallStart(types.FETCH_LISTS));

    return fetch(`${API_CONFIG.url}/lists`)
      .then(response => response.json())
      .then(lists => {
        dispatch(loadLists(lists));
        dispatch(ajaxCallSuccess(types.FETCH_LISTS));
      })
      .catch((err) => {
        dispatch(ajaxCallError(types.FETCH_LISTS, err));
      });
  };
}

export function loadLists(lists) {
  return {
    type: types.LOAD_LISTS,
    lists
  };
}

export function fetchList(id) {
  return dispatch => {
    dispatch(ajaxCallStart(types.FETCH_LIST));

    return fetch(`${API_CONFIG.url}/lists/${id}`)
      .then(response => response.json())
      .then(list => {
        dispatch(loadList(list));
        dispatch(ajaxCallSuccess(types.FETCH_LIST));
      })
      .catch(err => {
        dispatch(ajaxCallError(types.FETCH_LIST, err));
      });
  };
}

export function loadList(list) {
  return {
    type: types.LOAD_LIST,
    list
  };
}

export function addList(list) {
  return {
    type: types.ADD_LIST,
    list
  };
}

export function updateList(list) {
  return dispatch => {
    return fetch('/some/url', {
      method: 'POST',
      body: list
    })
      .then(response => response.json())
      .then(json => {
        return dispatch(loadList(json));
      });
  };
}

export function removeUser(id) {
  return dispatch => {
    return dispatch(removeUserSuccess(id));
  };
}

export function removeUserSuccess(id) {
  return {
    type: types.REMOVE_USER,
    id
  };
}
