import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../actions/userActions';
import SelectedUsers from '../components/SelectedUsers';
import AutoComplete from 'material-ui/AutoComplete';

class Subscriber extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      users: [],
      query: ''
    };

    this.selectUser = this.selectUser.bind(this);
    this.removeUser = this.removeUser.bind(this);
    this.filterUsers = this.filterUsers.bind(this);
    this.filterUsersByQuery = this.filterUsersByQuery.bind(this);
    this.findUsers = this.findUsers.bind(this);
  }

  componentWillMount() {
    this.findUsers('');

    this.setState({
      'formData': [...this.props.formData]
    });
  }

  filterUsers(users, selected) {
    const selectedUsers = selected.map(user => user.id);
    return users ? users.filter(user => selectedUsers.indexOf(user.id) < 0) : [];
  }

  filterUsersByQuery(searchText, key) {
    return !searchText || searchText && key.toLowerCase().indexOf(searchText.toLowerCase()) >= 0;
  }

  findUsers(searchText) {
    this.setState({
      'query': searchText
    });
    this.props.userActions.findUsers(searchText);
  }

  selectUser(user) {
    const selected = [...this.state.formData, this.props.users.find(usr => usr.id === user.value)];
    this.setState({
      'formData': selected,
      'query': ''
    }, () => this.props.onChange(this.state.formData));
    this.findUsers('');
  }

  removeUser(id) {
    const selected = [].concat(this.state.formData.filter(user => user.id !== id));
    this.setState({
      'formData': selected
    }, () => this.props.onChange(this.state.formData));
  }

  render() {
    const users = this.filterUsers(this.props.users, this.state.formData).map(user => Object.assign({text: `${user.name} (${user.email})`, value: user.id}));
    const autoCompleteStyle = {
      marginBottom: '1.5rem'
    };

    return (
      <div>
        <AutoComplete
          floatingLabelText={this.props.schema.title}
          openOnFocus={true}
          dataSource={users}
          onUpdateInput={this.findUsers}
          onNewRequest={this.selectUser}
          filter={this.filterUsersByQuery}
          searchText={this.state.query}
          menuCloseDelay={0}
          disabled={!users.length}
          style={autoCompleteStyle} />
        <SelectedUsers
          users={this.state.formData}
          onRemoveUser={this.removeUser} />
      </div>
    );
  }
}

Subscriber.propTypes = {
  formData: PropTypes.array.isRequired,
  userActions: PropTypes.object,
  users: PropTypes.array,
  onChange: PropTypes.func,
  schema: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        users: state.users
    };
}

function mapDispatchToProps(dispatch) {
    return {
        userActions: bindActionCreators(userActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Subscriber);
