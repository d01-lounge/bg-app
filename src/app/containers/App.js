import React, { Component, PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as contextActions from '../actions/contextActions';
import Main from '../components/Main';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar  from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Avatar from 'material-ui/Avatar';
import ActionEvent from 'material-ui/svg-icons/action/event';
import CommunicationEmail from 'material-ui/svg-icons/communication/email';
import ActionSettings from 'material-ui/svg-icons/action/settings';

class App extends Component {
  constructor(props, context) {
    super(props, context);

    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  toggleDrawer(override) {
    const visible = override !== undefined && typeof override === Boolean ? override : !this.props.contextState.drawer;
    this.props.contextActions.updateDrawer(visible);
  }

  render() {
    const logo = <Avatar src="../gfx/logo.svg" backgroundColor="#F3F3F3" size={48} />;
    const iconStyle = {
      top: '.375rem'
    };
    const titleStyle = {
      fontWeight: '300'
    };

    return (
      <MuiThemeProvider>
        <div>
          <AppBar titleStyle={titleStyle} title={this.props.contextState.title} onLeftIconButtonTouchTap={this.toggleDrawer} />
          <Drawer containerClassName="sidebar" docked={false} width={320} open={this.props.contextState.drawer} onRequestChange={(open) => this.toggleDrawer(open)}>
            <AppBar title={this.props.contextState.appName} titleStyle={titleStyle} iconElementLeft={logo} />
            <MenuItem onTouchTap={() => this.toggleDrawer(false)} leftIcon={<ActionEvent style={iconStyle} />}>
              <IndexLink to="/events">Events</IndexLink>
            </MenuItem>
            <MenuItem onTouchTap={() => this.toggleDrawer(false)} leftIcon={<CommunicationEmail style={iconStyle} />}>
              <Link to="/mailing-lists">Mailing Lists</Link>
            </MenuItem>
            <MenuItem onTouchTap={() => this.toggleDrawer(false)} leftIcon={<ActionSettings style={iconStyle} />}>
              <Link to="/settings">Settings</Link>
            </MenuItem>
          </Drawer>
          <Main children={this.props.children} />
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.element,
  contextState: PropTypes.object.isRequired,
  contextActions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    contextState: state.context
  };
}

function mapDispatchToProps(dispatch) {
  return {
    contextActions: bindActionCreators(contextActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
