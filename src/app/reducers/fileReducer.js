import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function file(state = initialState.file, action) {
  switch (action.type) {
    case types.LOAD_FILE:
      return Object.assign({}, action.file);
    default:
      return state;
  }
}
