import { MOBILE } from '../constants/media';

export default {
  header: {
    search: false
  },
  context: {
    title: 'Bright Giraffe',
    background: 'generic',
    sidebar: false,
    appName: 'Bright Giraffe',
    media: MOBILE
  },
  lists: [],
  events: [],
  event: {},
  list: {},
  users: [],
  file: {}
};
