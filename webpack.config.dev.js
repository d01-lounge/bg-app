const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const ENV = process.env.NODE_ENV || 'DEV';
const METADATA = {
  title: 'Bright Giraffe'
};

const root = (...args) => {
  return path.join.apply(path, [__dirname].concat(args));
};

module.exports = {
  devtool: 'eval-source-map',
  entry: {
    app: './src/app/index.js',
    vendor: [
      'babel-polyfill',
      'es6-promise',
      'immutable',
      'isomorphic-fetch',
      'react-dom',
      'react-redux',
      'react-router',
      'react',
      'redux-thunk',
      'redux'
    ]
  },
  resolve: {
      // only discover files that have those extensions
      extensions: [ '.ts', '.js', '.json', '.css', '.scss', '.html' ],
      modules: [
          root('node_modules'),
          root('src')
      ]
  },
  output: {
    path: root('dist'), // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: 'http://localhost:3000/', // Use absolute paths to avoid the way that URLs are resolved by Chrome when they're parsed from a dynamically loaded CSS blob. Note: Only necessary in Dev.
    filename: 'js/[name].js',
    chunkFilename: '[id].chunk.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      include: root('src'),
      loaders: ['babel-loader']
    }, {
      test: /\.eot(\?v=\d+.\d+.\d+)?$/,
      loader: 'file-loader'
    }, {
      test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: "url-loader",
      options: {
        limit: 10000,
        mimetype: 'application/font-woff'
      }
    }, {
      test: /\.ttf(\?v=\d+.\d+.\d+)?$/,
      loader: 'file-loader',
      options: {
        limit: 10000,
        mimetype: 'application/octet-stream'
      }
    }, {
      test: /\.svg(\?v=\d+.\d+.\d+)?$/,
      loader: 'file-loader',
      options: {
        limit: 10000,
        mimetype: 'image/svg+xml'
      }
    }, {
      test: /\.(jpe?g|png|gif)$/i,
      loaders: ['file-loader']
    }, {
      test: /\.ico$/,
      loader: 'file-loader',
      options: {
        name: '[name].[ext]'
      }
    }, {
      test: /(\.css|\.scss)$/,
      loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']
    }]
  },
  plugins: [
    new webpack.DefinePlugin({
        'ENV': JSON.stringify(ENV),
        'process.env': {
            'ENV': JSON.stringify(ENV),
            'NODE_ENV': JSON.stringify(ENV)
        }
    }),
    new HtmlWebpackPlugin({
      template: root('src/public/index.html'),
      title: METADATA.title,
      inject: true
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: ['vendor']
    })
  ],
  node: {
    dns: 'mock', // fetch required setting
    net: 'mock', // fetch required setting
    global: true,
    process: true,
    module: false,
    clearImmediate: false,
    setImmediate: false
  },
  devServer: {
    contentBase: './src/public',
    historyApiFallback: true,
    port: 3000,
    proxy: {
      '/api/**': {
        target: 'http://localhost:3030',
        pathRewrite: {
            '^/api': ''
        },
        secure: false
      }
    },
    quiet: true,
    stats: 'verbose' // none (or false), errors-only, minimal, normal (or true) and verbose
  }
};
