import ImagePicker from '../fields/ImagePicker';
import SchemaDateTimePicker from '../fields/SchemaDateTimePicker';
import LocationPicker from '../fields/LocationPicker';
// import AutoComplete from '../fields/AutoComplete';

export default {
  formSchema: {
    type: 'object',
    required: ['title', 'location', 'mailingList'],
    properties: {
      title: {
        type: 'string',
        title: 'Choose a name for your event',
        default: 'My new event'
      },
      description: {
        type: 'string',
        title: 'Give your event a description',
        default: 'Some stuff about my event'
      },
      image: {
        type: 'string',
        title: 'Choose a fitting image'
      },
      mailingList: {
        type: 'string',
        title: 'Choose a mailing list'
      },
      start: {
        type: 'string',
        title: 'When does your event start?'
      },
      end: {
        type: 'string',
        title: 'When does your event end?'
      },
      location: {
        type: 'object',
        title: 'Where does your event take place?',
        properties: {
          lat: {
            type: 'number',
            default: 0
          },
          lng: {
            type: 'number',
            default: 0
          },
          label: {
            type: 'string'
          }
        }
      }
    }
  },
  uiSchema: {
    image: {
      'ui:field': 'image'
    },
    start: {
      'ui:field': 'datetime'
    },
    end: {
      'ui:field': 'datetime'
    },
    location: {
      'ui:field': 'location'
    }
    // mailingList: {
    //   'ui:field': 'autocomplete'
    // }
  },
  fields: {
    'image': ImagePicker,
    'datetime': SchemaDateTimePicker,
    location: LocationPicker
    // 'autocomplete': AutoComplete
  }
};
