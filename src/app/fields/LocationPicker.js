import React, { PropTypes } from 'react';
import Geosuggest from 'react-geosuggest';

class LocationPicker extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onSelectPlace = this.onSelectPlace.bind(this);
  }

  componentWillMount() {
    this.setState({
      'formData': this.props.formData
    });
  }

  onSelectPlace(place) {
    this.setState({
      formData: {
        label: place.label,
        ...place.location
      }
    }, () => this.props.onChange(this.state.formData));
  }

  render() {
    return (
      <div>
        <Geosuggest
          ref={el => this._geoSuggest = el}
          placeholder="Search for a place"
          initialValue={this.state.formData.label || 'Veldkant, Kontich'}
          onSuggestSelect={this.onSelectPlace}
          location={new google.maps.LatLng(51.142015, 4.4401782)}
          radius="20" />
      </div>
    );
  }
}

LocationPicker.propTypes = {
  formData: PropTypes.object.isRequired,
  onChange: PropTypes.func,
  schema: PropTypes.object.isRequired
};

export default LocationPicker;
